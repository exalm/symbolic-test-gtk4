
#include "symbolic_test_gtk4-config.h"
#include "symbolic_test_gtk4-window.h"

struct _SymbolicTestGtk4Window
{
  GtkApplicationWindow  parent_instance;
};

G_DEFINE_TYPE (SymbolicTestGtk4Window, symbolic_test_gtk4_window, GTK_TYPE_APPLICATION_WINDOW)

static void
symbolic_test_gtk4_window_class_init (SymbolicTestGtk4WindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/App/symbolic_test_gtk4-window.ui");
}

static void
symbolic_test_gtk4_window_init (SymbolicTestGtk4Window *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
