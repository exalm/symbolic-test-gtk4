
#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define SYMBOLIC_TEST_GTK4_TYPE_WINDOW (symbolic_test_gtk4_window_get_type())

G_DECLARE_FINAL_TYPE (SymbolicTestGtk4Window, symbolic_test_gtk4_window, SYMBOLIC_TEST_GTK4, WINDOW, GtkApplicationWindow)

G_END_DECLS
